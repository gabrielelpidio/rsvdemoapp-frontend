import Loading from "../components/Loading/Loading";

export function asyncElement(element, { data, error, isLoading }) {
  if (isLoading) {
    return <Loading />;
  }
  if (error) {
    return `error ${error}`;
  }
  return element(data);
}
