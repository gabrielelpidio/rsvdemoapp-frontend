import { useEffect, useState } from "react";
import { useRouter } from "next/router";

export function useLayout() {
  const [usesLayout, setUsesLayout] = useState(false);
  const router = useRouter();
  useEffect(() => {
    fetch("/api/layout-routes")
      .then((res) => res.json())
      .then((data) => {
        if (data.routes.includes(router.pathname)) {
          setUsesLayout(true);
        } else {
          setUsesLayout(false);
        }
      });
  }, [router]);
  return usesLayout;
}
