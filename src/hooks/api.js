import { fetcher } from "../utils/api";
import useSWR from "swr";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

export const useUsers = () => {
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_API_URL}/api/user`,
    fetcher
  );

  return { data, error, isLoading: !data && !error };
};

export const useUser = (id) => {
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_API_URL}/api/user/${id}`,
    fetcher
  );

  return { data, error, isLoading: !data && !error };
};

export const useLoggedUser = () => {
  const { user } = useContext(AuthContext);
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_API_URL}/api/user/${user.id}`,
    fetcher
  );

  return { data, error, isLoading: !data && !error };
};
