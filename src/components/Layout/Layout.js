import React from "react";
import styles from "./Layout.module.css";
import { NavigationBar } from "../NavigationBar/NavigationBar";
import Header from "../Header/Header";

export const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Header>
        <NavigationBar />
      </Header>
      <main className={styles.main}>{children}</main>
    </div>
  );
};
