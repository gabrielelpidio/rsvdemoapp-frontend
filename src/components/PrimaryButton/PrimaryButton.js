import React from "react";
import Loading from "../Loading/Loading";
import styles from "./PrimaryButton.module.css";

export const PrimaryButton = ({ title, disabled, ...props }) => {
  return (
    <button className={styles.button} {...props} disabled={disabled}>
      {title}
    </button>
  );
};
