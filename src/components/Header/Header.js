import { useRouter } from "next/router";
import React, { useContext, useEffect } from "react";
import { useLoggedUser } from "../../hooks/api";
import { asyncElement } from "../../utils/asyncElement";
import styles from "./Header.module.css";

const Header = ({ children }) => {
  const router = useRouter();
  const { data, error, isLoading } = useLoggedUser();
  return (
    <header className={styles.header}>
      {children}
      <figure
        className={styles.brandImgContainer}
        onClick={() => router.push("/")}
      >
        <img src="/reserve.png" alt="" />
      </figure>
      <div className={styles.user} onClick={() => router.push("/profile")}>
        {asyncElement(
          () => (
            <>
              <span>{data.body[0].name}</span>
              <figure className={styles.userImg}>
                <img
                  src={`https://avatars.dicebear.com/api/gridy/${data.body[0].username}.svg`}
                  alt="avatar"
                />
              </figure>
            </>
          ),
          { data, error, isLoading }
        )}
      </div>
    </header>
  );
};

export default Header;
