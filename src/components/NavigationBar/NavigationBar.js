import React, { useEffect, useContext, useRef, useState } from "react";
import styles from "./NavigationBar.module.css";
import { animated, useSpring } from "react-spring";
import useMeasure from "react-use-measure";
import { NavigationLink } from "../NavigationLink/NavigationLink";
import {
  AiOutlineHome,
  AiOutlineLogout,
  AiOutlineUser,
  AiOutlineMenu,
  AiOutlineClose,
} from "react-icons/ai";
import { AuthContext } from "../../context/AuthContext";
import { useRouter } from "next/router";

export const NavigationBar = () => {
  const { actions } = useContext(AuthContext);
  const router = useRouter();

  const [open, setOpen] = useState(false);
  const [ref, bounds] = useMeasure();
  const { transform, opacity } = useSpring({
    transform: open ? `translateX(0px)` : `translateX(-${bounds.width}px)`,
    opacity: open ? 0.5 : 0,
  });

  return (
    <>
      <button
        className={styles.navMenuButton}
        onClick={() => setOpen((open) => !open)}
      >
        <AiOutlineMenu />
      </button>
      <animated.div
        className={styles.overlay}
        onClick={() => setOpen(false)}
        style={{
          opacity,
          display: opacity
            .interpolate([0, 0.5], [0, 1])
            .interpolate((n) => (n > 0.05 ? "block" : "none")),
        }}
      ></animated.div>
      <animated.nav className={styles.nav} ref={ref} style={{ transform }}>
        <button
          className={styles.navCloseButton}
          onClick={() => setOpen(false)}
        >
          <AiOutlineClose />
        </button>
        <NavigationLink href="/">
          <AiOutlineHome />
          Home
        </NavigationLink>
        <NavigationLink href="/users">
          <AiOutlineUser />
          Users
        </NavigationLink>
        <NavigationLink
          onClick={() => {
            actions.logout();
            router.push("/signin");
          }}
        >
          <AiOutlineLogout />
          Logout
        </NavigationLink>
      </animated.nav>
    </>
  );
};
