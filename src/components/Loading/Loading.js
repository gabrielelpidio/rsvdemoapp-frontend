import React from "react";
import styles from "./Loading.module.css";

const Loading = ({ size = "65px", className, stroke }) => {
  return (
    <div className={styles.loader + className}>
      <svg
        className={styles.spinner}
        width={size}
        height={size}
        viewBox="0 0 66 66"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle
          className={styles.path}
          stroke={stroke}
          fill="none"
          strokeWidth="6"
          strokeLinecap="round"
          cx="33"
          cy="33"
          r="30"
        ></circle>
      </svg>
    </div>
  );
};

export default Loading;
