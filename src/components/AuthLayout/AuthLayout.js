import React from "react";
import styles from "./AuthLayout.module.css";

export const AuthLayout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <header className={styles.header}>
        <figure className={styles.brandImgContainer}>
          <img src="/reserve.png" alt="" />
        </figure>
      </header>
      {children}
    </div>
  );
};
