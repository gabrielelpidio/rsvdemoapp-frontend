import React, { useEffect, useState } from "react";
import { PrimaryButton } from "../PrimaryButton/PrimaryButton";
import styles from "./Form.module.css";

export const Form = ({
  children,
  title,
  inputs = [{}],
  actionButtonTitle,
  onSubmit,
  error,
  message,
  ...props
}) => {
  const [inputsData, setInputsData] = useState(inputs);
  const [disabled, setDisabled] = useState(false);

  function handleInput(e) {
    setInputsData((inputs) => {
      return {
        ...inputs,
        [e.target.name]: e.target.value,
      };
    });
  }

  function handleSubmit(e) {
    e.preventDefault();
    setDisabled(true);
    onSubmit(inputsData);
    setDisabled(false);
  }

  return (
    <div className={styles.formWrapper}>
      {error && <h2 className={styles.formError}>{error.message}</h2>}
      {message && <h2 className={styles.formMessage}>{message}</h2>}
      {title && <h1 className={styles.formTitle}>{title}</h1>}
      <form
        action=""
        {...props}
        className={styles.form}
        onSubmit={handleSubmit}
      >
        {inputs &&
          inputs.map((i) => (
            <input
              type={i.type || "text"}
              placeholder={i.placeholder || ""}
              name={i.name || ""}
              className={styles.input}
              onChange={handleInput}
              required={i.required ? true : false}
              disabled={disabled}
              key={i.name}
            ></input>
          ))}
        <PrimaryButton
          disabled={disabled}
          type="submit"
          title={actionButtonTitle}
        ></PrimaryButton>
      </form>
      {children}
    </div>
  );
};
