import React from "react";
import { useRouter } from "next/router";
import styles from "./ProfileContent.module.css";
import { PrimaryButton } from "../PrimaryButton/PrimaryButton";
import { ProfileLayout } from "../ProfileLayout/ProfileLayout";
import { useLoggedUser, useUser } from "../../hooks/api";
import { asyncElement } from "../../utils/asyncElement";

const ProfileContent = () => {
  const router = useRouter();
  const { data, error, isLoading } = useLoggedUser();
  return (
    <ProfileLayout>
      <main className={styles.main}>
        {asyncElement(
          () => {
            const user = data.body[0];
            return (
              <>
                <h1>{user.name}</h1>
                <h2>{user.username}</h2>
                <PrimaryButton
                  title="Edit account"
                  onClick={() => router.push("/profile/edit")}
                ></PrimaryButton>{" "}
              </>
            );
          },
          { data, error, isLoading }
        )}
      </main>
    </ProfileLayout>
  );
};

export default ProfileContent;
