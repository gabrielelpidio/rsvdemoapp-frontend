import React from "react";
import styles from "./ProfileLayout.module.css";
import { AiOutlineHome } from "react-icons/ai";
import Header from "../Header/Header";
import { useRouter } from "next/router";

export const ProfileLayout = ({ children }) => {
  const router = useRouter();
  return (
    <div className={styles.layout}>
      <Header>
        <button
          className={styles.HeaderActionWrapper}
          onClick={() => router.push("/")}
        >
          <AiOutlineHome></AiOutlineHome>
          Home
        </button>
      </Header>
      {children}
    </div>
  );
};
