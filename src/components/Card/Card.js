import React from "react";
import styles from "./Card.module.css";

export const Card = ({ title, actionButton, action, onClick, children }) => {
  return (
    <div className={styles.wrapper}>
      <h2>{title}</h2>
      <article className={styles.card}>
        <div className={styles.content}>{children}</div>
        {actionButton && (
          <button className={styles.actionButton} onClick={onClick}>
            {action}
          </button>
        )}
      </article>
    </div>
  );
};
