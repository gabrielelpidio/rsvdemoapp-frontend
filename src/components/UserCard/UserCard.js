import React from "react";
import { Card } from "../Card/Card";
import styles from "./UserCard.module.css";

export const UserCard = ({ name, username, ...props }) => {
  return (
    <Card {...props}>
      <div className={styles.profileWrapper}>
        <figure className={styles.profileImg}>
          <img
            src={`https://avatars.dicebear.com/api/gridy/${username}.svg`}
            alt="avatar"
          />
        </figure>
        <h3>{name}</h3>
        <span>{username} </span>
      </div>
    </Card>
  );
};
