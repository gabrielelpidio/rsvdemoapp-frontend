import React from "react";
import { Card } from "../Card/Card";
import styles from "./HomeDashboard.module.css";
import { AiOutlineRight } from "react-icons/ai";
import { useRouter } from "next/router";
import { UserCard } from "../UserCard/UserCard";
import { useLoggedUser, useUsers } from "../../hooks/api";
import { asyncElement } from "../../utils/asyncElement";

const HomeDashboard = () => {
  const router = useRouter();
  const users = useUsers();
  const user = useLoggedUser();
  return (
    <section className={styles.dashboard}>
      <Card
        actionButton={true}
        action="Show more"
        title="Users"
        asyncData={users}
        asyncContent={true}
        onClick={() => router.push("/users")}
      >
        <ul className={styles.userList}>
          {asyncElement(
            (data) =>
              data.body.slice(0, 4).map((i) => (
                <li key={i.id} className={styles.userListItem}>
                  <figure className={styles.userListItem_imgWrapper}>
                    <img
                      src={`https://avatars.dicebear.com/api/gridy/${i.username}.svg`}
                      alt="avatar"
                    />
                  </figure>
                  {i.name}
                </li>
              )),
            users
          )}
        </ul>
      </Card>
      {asyncElement((data) => {
        const user = data.body[0];
        return (
          <UserCard
            actionButton={true}
            action="See profile"
            title="My profile"
            username={user.username}
            name={user.name}
            onClick={() => router.push("/profile")}
          ></UserCard>
        );
      }, user)}
    </section>
  );
};

export default HomeDashboard;
