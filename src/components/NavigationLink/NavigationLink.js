import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "./NavigationLink.module.css";

export const NavigationLink = ({ children, href = "", onClick }) => {
  const [focus, setFocus] = useState(false);
  const router = useRouter();
  useEffect(() => {
    if (router.pathname === href) {
      setFocus(true);
    } else {
      setFocus(false);
    }
  }, [router]);

  return href ? (
    <Link href={href}>
      <a className={`${styles.link} ${focus ? styles.active : ""}`}>
        {children}
      </a>
    </Link>
  ) : (
    <button
      className={`${styles.link} ${focus ? styles.active : ""}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
