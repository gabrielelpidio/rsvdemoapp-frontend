import React from "react";
import styles from "./UserContent.module.css";

export const UsersContent = ({ children }) => {
  return <div className={styles.layout}>{children}</div>;
};
