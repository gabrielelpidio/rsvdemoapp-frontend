import React, { useEffect, useReducer, useState } from "react";

const storageKey = "localUser";
const INITIAL_VALUE = { isLogged: false, token: "", id: "" };
export const AuthContext = React.createContext(INITIAL_VALUE);

const mapDispatch = (dispatch) => ({
  login: (payload) =>
    dispatch({
      type: "login",
      payload: { id: payload.id, token: payload.token, isLogged: true },
    }),
  logout: () => dispatch({ type: "logout" }),
});

function reducer(state, action) {
  switch (action.type) {
    case "login":
      return action.payload;
    case "logout":
      return INITIAL_VALUE;
    default:
      break;
  }
}

export const Provider = ({ children }) => {
  const [initialized, setInitialized] = useState(false);
  const [state, dispatch] = useReducer(reducer, INITIAL_VALUE);
  const actions = mapDispatch(dispatch);

  //Avoid rewriting localStorage from being in SSR
  useEffect(() => {
    if (typeof window !== "undefined") {
      setInitialized(true);
    }
  }, []);

  useEffect(() => {
    if (initialized) {
      actions.login(JSON.parse(localStorage.getItem(storageKey)));
    }
  }, [initialized]);

  useEffect(() => {
    if (initialized) {
      localStorage.setItem(storageKey, JSON.stringify(state));
    }
  }, [state, initialized]);

  return (
    <AuthContext.Provider value={{ user: state, actions }}>
      {children}
    </AuthContext.Provider>
  );
};
