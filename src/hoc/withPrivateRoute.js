import React, { useContext, useEffect } from "react";
import Router, { useRouter } from "next/router";
import { AuthContext } from "../context/AuthContext";

const login = "/login?redirected=true";
const storageKey = "localUser";

const checkUserAuthentication = () => {
  return JSON.parse(localStorage.getItem(storageKey))?.token !== "";
};

const HOC = (WrappedComponent) => {
  const Component = ({ ...props }) => {
    const router = useRouter();
    useEffect(() => {
      console.log(checkUserAuthentication());
      if (!checkUserAuthentication()) {
        router.push("/signin");
      }
    }, []);
    return <WrappedComponent {...props} />;
  };

  if (Component.getInitialProps) {
    WrappedComponent.getInitialProps = Component.getInitialProps;
  }

  return Component;
};

export default HOC;
