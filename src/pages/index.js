import Head from "next/head";
import HomeDashboard from "../components/HomeDashboard/HomeDashboard";
import withPrivateRoute from "../hoc/withPrivateRoute";

function Home() {
  return (
    <>
      <HomeDashboard></HomeDashboard>
    </>
  );
}

export default withPrivateRoute(Home);
