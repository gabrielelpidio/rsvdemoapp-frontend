import React, { useContext, useEffect, useState } from "react";
import { AuthLayout } from "../components/AuthLayout/AuthLayout";
import { Form } from "../components/Form/Form";
import Link from "next/link";
import { AuthContext } from "../context/AuthContext";
import { useRouter } from "next/router";

const INPUT_VALUES = [
  { type: "text", name: "username", placeholder: "Username", required: true },
  {
    type: "password",
    name: "password",
    placeholder: "Password",
    required: true,
  },
];

const Singin = () => {
  const { user, actions } = useContext(AuthContext);
  const [error, setError] = useState(false);
  const router = useRouter();

  function handleSubmit(data) {
    fetch(process.env.NEXT_PUBLIC_API_URL + "/api/auth/login", {
      body: JSON.stringify({
        username: data.username,
        password: data.password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    })
      .then((res) => {
        if (!res.ok) {
          if (res.status === 401) {
            throw new Error("Invalid username or password");
          } else {
            throw new Error("Authentication Error");
          }
        }
        return res.json();
      })
      .then((res) => {
        actions.login({ id: res.body.id, token: res.body.token });
        router.push("/");
      })
      .catch((err) => {
        setError(err);
      });
  }
  useEffect(() => {
    console.log(router.query.redirectFromSignup);
  }, [router]);

  return (
    <AuthLayout>
      <Form
        inputs={INPUT_VALUES}
        actionButtonTitle="Sign In"
        onSubmit={handleSubmit}
        message={
          router.query?.redirectFromSignup
            ? "Account created successfully, log in to access the app"
            : ""
        }
        error={error}
      >
        {"I don't have an account"}
        <Link href="/signup">
          <a>Sign Up</a>
        </Link>
      </Form>
    </AuthLayout>
  );
};

export default Singin;
