import { Layout } from "../components/Layout/Layout";
import { Provider } from "../context/AuthContext";
import { useLayout } from "../hooks/useLayout";

import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  const usesLayout = useLayout();
  return (
    <Provider>
      {usesLayout ? (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      ) : (
        <Component {...pageProps} />
      )}
    </Provider>
  );
}

export default MyApp;
