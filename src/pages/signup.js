import React, { useContext, useState } from "react";
import { AuthLayout } from "../components/AuthLayout/AuthLayout";
import { Form } from "../components/Form/Form";
import Link from "next/link";
import { AuthContext } from "../context/AuthContext";
import { useRouter } from "next/router";

const INPUT_VALUES = [
  { type: "text", name: "name", placeholder: "Name", required: true },
  { type: "text", name: "username", placeholder: "Username", required: true },
  {
    type: "password",
    name: "password",
    placeholder: "Password",
    required: true,
  },
];

const Singup = () => {
  const { user, actions } = useContext(AuthContext);
  const [error, setError] = useState(false);

  const router = useRouter();
  function handleSubmit(data) {
    fetch(process.env.NEXT_PUBLIC_API_URL + "/api/user", {
      body: JSON.stringify({
        name: data.name,
        username: data.username,
        password: data.password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error("Error Signing up, try again");
        }
        return res.json();
      })
      .then((res) => router.push("/signin?redirectFromSignup=true"))
      .catch((err) => {
        setError(err);
      });
  }

  return (
    <AuthLayout>
      <Form
        inputs={INPUT_VALUES}
        actionButtonTitle="Sign Up"
        onSubmit={handleSubmit}
        error={error}
      >
        {"I have an account"}
        <Link href="/signin">
          <a>Sign In</a>
        </Link>
      </Form>
    </AuthLayout>
  );
};

export default Singup;
