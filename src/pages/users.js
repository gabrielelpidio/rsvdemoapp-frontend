import React, { useEffect } from "react";
import { render } from "react-dom";
import Loading from "../components/Loading/Loading";
import { UserCard } from "../components/UserCard/UserCard";
import { UsersContent } from "../components/UsersContent/UsersContent";
import withPrivateRoute from "../hoc/withPrivateRoute";
import { useUsers } from "../hooks/api";

const Users = () => {
  const { data, error, isLoading } = useUsers();
  function renderElement() {
    if (isLoading) {
      return <Loading />;
    }
    if (error) {
      return `error`;
    }
    return data.body.map((i) => (
      <UserCard name={i.name} username={i.username} />
    ));
  }

  return <UsersContent>{renderElement()}</UsersContent>;
};

export default withPrivateRoute(Users);
