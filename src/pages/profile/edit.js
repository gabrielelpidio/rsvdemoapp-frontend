import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { Form } from "../../components/Form/Form";
import { PrimaryButton } from "../../components/PrimaryButton/PrimaryButton";
import { ProfileLayout } from "../../components/ProfileLayout/ProfileLayout";
import { AuthContext } from "../../context/AuthContext";
import withPrivateRoute from "../../hoc/withPrivateRoute";
import { useLoggedUser } from "../../hooks/api";
import { asyncElement } from "../../utils/asyncElement";
const INPUT_VALUES = [
  { type: "text", name: "name", placeholder: "New Name", required: true },
  {
    type: "text",
    name: "username",
    placeholder: "New Username",
    required: true,
  },
  {
    type: "password",
    name: "password",
    placeholder: "New Password",
    required: true,
  },
];

const Edit = () => {
  const [inputs, setInputs] = useState(INPUT_VALUES);
  const { user, actions } = useContext(AuthContext);
  const [formError, setFormError] = useState(false);
  const { data, error, isLoading } = useLoggedUser();

  const router = useRouter();
  useEffect(() => {
    if (data) {
      setInputs((i) =>
        i.map((e) => {
          if (data.body[0][e.name]) {
            return { ...e, placeholder: data.body[0][e.name] };
          } else {
            return e;
          }
        })
      );
    }
  }, [data]);

  function handleSubmit(formData) {
    fetch(process.env.NEXT_PUBLIC_API_URL + "/api/user", {
      body: JSON.stringify({ ...formData, id: user.id }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      method: "PUT",
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error("Error updating the user, try again");
        }
        return res.json();
      })
      .then((res) => router.push("/profile"))
      .catch((err) => {
        setFormError(err);
      });
  }

  function handleDelete() {
    fetch(process.env.NEXT_PUBLIC_API_URL + "/api/user/" + user.id, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
      method: "DELETE",
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error("Error deleting the user, try again");
        }
        return res.json();
      })
      .then(() => {
        actions.logout();
        router.push("/signin");
      })
      .catch((err) => {
        setFormError(err);
      });
  }

  return (
    <ProfileLayout>
      {asyncElement(
        () => (
          <Form
            title="Edit profile"
            inputs={inputs}
            actionButtonTitle="Save changes"
            onSubmit={handleSubmit}
          >
            Or
            <PrimaryButton
              style={{ background: "#E5190C" }}
              title="Delete Account"
              onClick={handleDelete}
            ></PrimaryButton>
          </Form>
        ),
        { data, error, isLoading }
      )}
    </ProfileLayout>
  );
};

export default withPrivateRoute(Edit);
