import React from "react";

import ProfileContent from "../../components/ProfileContent/ProfileContent";
import withPrivateRoute from "../../hoc/withPrivateRoute";

const Profile = () => {
  return <ProfileContent></ProfileContent>;
};

export default withPrivateRoute(Profile);
